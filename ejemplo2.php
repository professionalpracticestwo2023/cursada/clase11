<?php
require('./fpdf/fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    /// Image(imagen,x,y,tamaño)
    $this->Image('logo.jpeg',10,8,20);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(80);
    // Título
/// Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]] 
/// Cell(cuadro-->> 0,10,texto,borde,posicion,alineacion,relleno)    
// 0 -> sin borde
// por defecto muchas opciones vienen en false    
//$this->Cell(50,10,'Titulo de ejemplo',1,1,'C',true);
    $this->Cell(50,10,'Titulo de ejemplo',1,1,'C');
    // Salto de línea
    $this->Ln(20);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);
for($i=1;$i<=40;$i++)
{
    $pdf->Cell(0,10,utf8_decode('Imprimiendo línea número ').$i,0,1);
}
$pdf->Output();
?>